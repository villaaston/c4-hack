var http = require('http'),
    parser = require('sami-parser'),
    sentiment = require('sentiment'),
    fs = require('fs'),
    uuid = require('node-uuid'),
    underscore = require('underscore')._;


exports.index = function(req, res){
    return res.render('index');
};

exports.postImage = function(req, res){

    var imageData = req.body.image.replace(/^data:image\/png;base64,/,'');
    var extension = req.body.extension;

    var filename = uuid.v1() + '.' + extension;

    console.log(__dirname);

    fs.writeFile(__dirname + '/../public/uploaded-images/' + filename, imageData, 'base64', function(err) {
        console.log(err);
        return res.json({
            error: err
        })
    });

    res.json({
        url: '/uploaded-images/' + filename
    })
};

exports.worm = function(req, res){
    getSentiment(req.params.progid, req.params.minimum, function(results){

        var sentiment = [];
        var subtitles = [];


        results.forEach(function(result){

                sentiment.push([result.startTime / 10000, result.score * 10]);
                subtitles.push({key: result.startTime, value: result.subtitle });
        });

        var wormData = [
            { key: 'Sentiment', values :sentiment }];

        res.render('worm', { layout: null, subtitles: subtitles, wormData: wormData, results: underscore.filter(results, function(result){
            return Math.abs(result.score) > 7;
        })});
    });

}


exports.sentiment = function(req, res){
   getSentiment(req.params.progid, req.params.minimum, function(results){
       res.json(results);
   });
}

function getSentiment(progid, minimum, callback){
    http.get({
        host: 'ais.channel4.com',
        path: '/subtitles/' + progid
    })
        .on('response', function(response) {

            var results = [];

            var minimumSentimentScore = minimum;

            response.setEncoding('utf8');

            response.on('data', function(chunk){

                console.log('chunk recieved ' + chunk);

                parser.parse(chunk).result.forEach(function(item){

                    if(item.languages.en){
                        sentiment(item.languages.en, function(err, result){
                            if(Math.abs(result.score) >= minimumSentimentScore){
                                results.push({
                                    startTime: item.startTime,
                                    subtitle: item.languages.en,
                                    score: result.score
                                });
                            }
                        });
                    }
                });
            });

            response.on('end', function(){
                callback(underscore.sortBy(results, 'startTime'));
            });
        });
}

exports.wit = require('./wit');
