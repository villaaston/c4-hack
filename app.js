
/**
 * Module dependencies.
 */

var express = require('express'),
    routes = require('./routes'),
    http = require('http'),
    path = require('path'),
    hbs = require('express-hbs');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.engine('hbs', hbs.express3({
    partialsDir: __dirname + '/views/partials',
    contentHelperName: 'content',
    defaultLayout: __dirname + '/views/layout.hbs'
}));
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

hbs.registerHelper('json', function(json){
    return JSON.stringify(json);
});

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/sentiment/:progid/:minimum', routes.sentiment);
app.get('/', routes.index);
app.post('/save-image', routes.postImage);
app.get('/wit/', routes.wit.proxy);
app.get('/worm/:progid/:minimum', routes.worm);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
